<?php
require "./include/general.php";
?>
<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<?php include "./include/head.php"; ?>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <a href="index.php" class="logo"><strong>Vše o PC</strong> - Ondřej Voves</a>
                </header>

                <section>
                    <header class="main">
                        <h1>Zdroje</h1>
                        <p>Na této stránce najdete použité zdroje</p>
                    </header>

                    <hr class="major" />

                    <h2>Historie PC a komponenty</h2>
                    <p>Historie počítačů [online]. Alza: Alza, 2022 [cit. 2023-08-28]. Dostupné z: https://www.alza.cz/historie-pocitacu?kampan=adwalz_alza_bee_gen_clanky_alza-clanek-historie-pocitacu-tema-36442&ppcbee-adtext-variant=pruvodce-vyberem-co-je-clanek&gclid=Cj0KCQjwi7GnBhDXARIsAFLvH4mjs63FV6OM6D2rTV87qt-5V6MZNM4f7rlyMUEyqAx058tGopKlxIUaArceEALw_wcB</p>
                    <h2>Obrázky</h2>
                    <p>Obrázky jsou z https://commons.wikimedia.org/</p>
                </section>

            </div>
        </div>

        <?php include "./include/side_nav.php"; ?>

    </div>

    <?php include "./include/scripts.php"; ?>
</body>

</html>