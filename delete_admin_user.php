<?php

require "./include/general.php";
require "./include/admin_guard.php";
require "./include/database.php";

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $id = mysqli_real_escape_string($conn, $_GET['id']);
    mysqli_query($conn, "DELETE FROM users WHERE id='$id'");

    header("Location: ./user_list.php");
    die();
}
