<?php
require "./../include/general.php";
?>
<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<?php include "./../include/head.php"; ?>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <a href="./../index.php" class="logo"><strong>Vše o PC</strong> - Ondřej Voves</a>
                </header>

                <section>
                    <header class="main">
                        <h1>CPU - Procesory</h1>
                        <p>Procesor je pomyslným mozkem celého počítače. Díky výběru vhodného modelu urychlíte běh náročných aplikací,
                            vymáčknete maximum z vaší grafické karty a minimalizujete spotřebu elektrické energie.
                            Na jaké důležité parametry a vlastnosti procesoru se tedy zaměřit? Čtěte dále.</p>

                    </header>

                    <hr class="major" />



                    <h2>Základní parametry procesorů</h2>
                    <p>Při výběru nového procesoru vás čeká hned několik klíčových rozhodnutí. Seznamte se proto níže s nejdůležitějšími
                        parametry a vlastnostmi,
                        kterými počítačové CPU oplývají.</p>
                    <h2>Procesory a počet jader</h2>
                    <p>Počet fyzických jader procesoru označuje počet samostatných výpočetních jednotek.
                        Čím více má procesor jader, tím více operací dokáže zpracovávat najednou neboli paralelně.
                        Obecně platí, že vyšší počet jader je lepší, Podívejte se na procesory podle počtu fyzických jader.</p>
                    <h2>Co udává pracovní frekvence procesoru?</h2>
                    <p>Frekvence je reálný parametr definující základní rychlost procesoru, pokud není ovlivněn vnějším zásahem.
                        Hlavní jednotkou je Hertz a v kontextu procesorů používáme jednotky gigahertzů čili miliardy Hertzů.</p>
                    <h2>Některé procesory lze přetaktovat</h2>
                    <p>S frekvencí CPU souvisí tzv. taktování.
                        Automatické či manuální přetaktování umožňuje zvýšit maximální frekvenci procesoru nad výrobcem stanovený limit.
                        Intel tuto technologii nazývá Turbo Boost, AMD potom Turbo Core. Ne všechny procesory jí však disponují.</p>
                    <h2>HyperThreading a Multi-Threading</h2>
                    <p>HyperThreading je technologie společnosti Intel, díky které dokáže jedno jádro procesoru zpracovávat dvě
                        softwarové operace najednou.
                        Do jisté míry je tedy procesor schopen pracovat, jako by měl dvojnásobný počet jader. Společnost AMD tuto
                        technologii nazývá Multi-Threading.
                    </p>
                    <h2>CPU a cache paměti</h2>
                    <p>Procesory disponují rychlými cache pamětmi L2 a L3.
                        Jejich úkolem je zvyšovat výkon celého systému zvětšením propustnosti a zrychlením přístupu k datům. Velikosti
                        L2 a L3 se pohybují od jednotek po stovky megabytů.</p>
                    <ul>
                        <li><b>L2 cache</b> je vyrovnávací paměť procesoru. Stojí mezi operační pamětí a výpočetními jednotkami
                            procesoru,
                            aby zamezila snižování rychlosti procesoru rychlostí paměti. Každé jádro má samostatnou L2 cache.</li>
                        <li><b>L3 cache</b> má paměť jen jednu a sdílí ji všechna jádra. Obecně platí, že čím větší cache, tím lépe.
                        </li>
                    </ul>
                    <h2>Socket neboli patice procesory</h2>
                    <p>Socket neboli patice je hardwarový konektor procesoru, do kterého se samotný čip usazuje.
                        Procesor a základní deska musí mít socket označený stejně, je to první krok k funkční sestavě.
                    </p>
                    <h2>Procesory s integrovanou grafickou kartou</h2>
                    <p>Procesory s integrovanou grafickou kartou pro běžnou kancelářskou práci nevyžadují dedikovanou grafickou kartu.
                        Základní grafický čip totiž mají integrovaný. Jedná se o ideální variantu pro úsporné sestavy, pro náročné
                        grafické úkony je ovšem lepší pořídit si dedikovanou kartu.</p>
                    <img src="./../obrazky/intel-procesor.jpg" alt="Procesor od Intelu">
                    <img src="./../obrazky/ryzen-procesor.jpg" alt="Procesor od Ryzenu">
                </section>

            </div>
        </div>

        <?php include "./../include/side_nav.php"; ?>

    </div>

    <?php include "./../include/scripts.php"; ?>
</body>

</html>