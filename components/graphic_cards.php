<?php
require "./../include/general.php";
?>
<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<?php include "./../include/head.php"; ?>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <a href="./../index.php" class="logo"><strong>Vše o PC</strong> - Ondřej Voves</a>
                </header>

                <section>
                    <header class="main">
                        <h1>Grafické karty</h1>
                    </header>

                    <hr class="major" />
                    <p>Grafické karty tvoří nezbytnou součást počítačů či notebooků, neboť se spolu s monitorem starají o zobrazení obrazu.
                        Grafické karty mohou být integrovány přímo na základní desce nebo existují externí grafické karty,
                        které se dělí na herní a profesionální a dle výrobce čipu na AMD (ATI) a NVIDIA.
                        Výkon grafické karty se odvíjí od frekvence, počtu a typu jáder (GPU), typu a velikosti grafické paměti nebo šířky
                        paměťové sběrnice.
                        Některé grafické karty umožňují podporu 3D zobrazení, což se promítne do 3D filmů, snímků a her, které pak uživatel
                        může sledovat a hrát na monitoru.</p>
                    <p>Grafické karty mají na zadní straně konektory pro připojení k monitoru.
                        Jedná se o analogové VGA nebo digitální DVI, HDMI a Display Port konektory. K desce s připojují standardně přes
                        PCI-Express x16 slot.
                        Některé grafické karty disponují pasivním chlazením, nicméně většina výkonnějších grafických karet je vybavena
                        aktivním chladičem pro dlouhodobý chod.
                        Mezi nejznámější výrobce grafických karet patří ASUS, EVGA, Gainward, GigaByte, MSI, Sapphire, Zotac.</p>

                </section>

            </div>
        </div>

        <?php include "./../include/side_nav.php"; ?>

    </div>

    <?php include "./../include/scripts.php"; ?>
</body>

</html>