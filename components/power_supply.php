<?php
require "./../include/general.php";
?>
<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<?php include "./../include/head.php"; ?>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <a href="./../index.php" class="logo"><strong>Vše o PC</strong> - Ondřej Voves</a>
                </header>

                <section>
                    <header class="main">
                        <h1>PC zdroje</h1>
                        <p>Počítačový zdroj slouží ke zpracování vstupního napětí z elektrické sítě, jeho transformaci a rozdělení do
                            napájecích větví.
                            Výstupem zdroje je několik napěťových úrovní distribuovaných skrze různé konektory. Nevíte, na jaké parametry se
                            při výběru PC zdroje zaměřit? Poradíme vám.</p>
                    </header>

                    <hr class="major" />

                    <h2>PC zdroje – dělení</h2>
                    <p>PC zdroje se podle typu využití dělí na pět hlavních kategorií.</p>
                    <h2>Obyčejné ATX zdroje a ještě menší SFX</h2>
                    <p>ATX PC zdroje poslouží všude tam, kde nepotřebujete nic extra speciálního – když váš počítač není primárně
                        stavěný za účelem hraní her,
                        těžení kryptoměn a nejste ani fanda do PC tuningu. Chystáte-li se pořídit zdroj do velmi kompaktní kancelářské
                        sestavy,
                        patrně vás budou víc zajímat SFX zdroje – tedy rozměrově menší bráškové ATX.</p>
                    <h2>Herní PC zdroje</h2>
                    <p>Herní PC zdroje mají vysoký výkon od 400 až do 2 000 W a v drtivé většině případů je seženete ve standardním
                        rozměru ATX,
                        díky čemuž zvládnou napájet všechny herní komponenty.
                        Často obsahují více konektorů vhodných pro zapojení více disků nebo grafických karet.</p>
                    <h2>PC zdroje pro těžbu kryptoměn</h2>
                    <p>PC zdroje k těžbě kryptoměny mají podobný či vyšší výkon než herní PC zdroje.
                        Zásadním rozdílem však je patrně vyšší počet PCI-E konektorů,
                        určených pro napájení více grafických karet najednou.</p>
                    <h2>PC zdroje pro tuning</h2>
                    <p>Zdroje pro PC tuning jsou převážně pořizovány z estetických důvodů.
                        Většinou se jedná o tiché zdroje vhodné pro PC skříně s RGB osvětlením. Pro lepší organizaci kabelů poslouží
                        modulární kabeláž,
                        jež může být součástí balení.</p>
                    <h2>Modulární PC zdroje</h2>
                    <p>Modulární zdroje se od běžných zdrojů liší odpojitelnými kabely, díky kterým získáte plnou kontrolu nad
                        organizací PC skříně.
                        Modulární kabely mohou pomoct proudění vzduchu, a do jisté míry tak omezit usazování prachu a nadměrnému
                        zvyšování teploty.</p>
                    <h2>PC zdroj musí mít správný výkon</h2>
                    <p>Výkon PC zdroje udává maximální množství Wattů, kterými je zdroj schopen souvisle zásobovat komponenty v
                        počítači.
                        Nevíte si rady, jaký potřebujete výkon? V tabulce naleznete přibližný potřebný výkon dle využití.
                        A pokud chcete mít přesnější představu, mrkněte na nejpopulárnější kalkulačku výkonu PC zdroje na
                        outervision.com.</p>
                    <table>
                        <tr>
                            <th>Typ zdroje</th>
                            <th>Využití</th>
                        </tr>
                        <tr>
                            <td>
                                <p>PC zdroj do 350W</p>
                            </td>
                            <td>
                                <p>Kancelářské počítače</p>
                            </td>
                        </tr>
                        <tr>
                            <td>PC zdroj 400-450W</td>
                            <td>Herní počítače a pracovní stanice nižší třídy.</td>
                        </tr>
                        <tr>
                            <td>PC zdroj 500-550W</td>
                            <td>Herní počítače a pracovní stanice střední třídy.</td>
                        </tr>
                        <tr>
                            <td>PC zdroj 600-650W</td>
                            <td>Herní počítače a pracovní stanice vyšší třídy.</td>
                        </tr>
                        <tr>
                            <td>PC zdroj 700-900W</td>
                            <td>Herní počítače a pracovní stanice nejvyšší třídy.</td>
                        </tr>
                        <tr>
                            <td>PC zdroj nad 1000W</td>
                            <td>Počítače na těžbu kryptoměn nebo serverové počítače.</td>
                        </tr>
                    </table>
                    <h2>Účinnost PC zdroje</h2>
                    <p>Parametr účinnosti určuje, kolik energie se ztratí a změní v teplo a kolik se rozdělí mezi komponenty počítače.
                        Zavedená certifikace 80 PLUS značí, které PC zdroje disponují účinností nad 80 %. V tabulce naleznete přehled
                        účinnosti při různých zatíženích.</p>
                    <table>
                        <tr>
                            <th>Zatížení PC zdroje</th>
                            <th>Účinnost při 50% zatížení</th>
                            <th>Účinnost při 100% zatížení</th>
                        </tr>
                        <tr>
                            <td>80 PLUS</td>
                            <td>85%</td>
                            <td>82%</td>
                        </tr>
                        <tr>
                            <td>80 PLUS Bronze</td>
                            <td>88%</td>
                            <td>85%</td>
                        </tr>
                        <tr>
                            <td>80 PLUS Silver</td>
                            <td>90%</td>
                            <td>87%</td>
                        </tr>
                        <tr>
                            <td>80 PLUS Gold</td>
                            <td>92%</td>
                            <td>89%</td>
                        </tr>
                        <tr>
                            <td>80 PLUS Platinum</td>
                            <td>94%</td>
                            <td>90%</td>
                        </tr>
                        <tr>
                            <td>80 PLUS Titanium</td>
                            <td>96%</td>
                            <td>94%</td>
                        </tr>
                    </table>
                </section>

            </div>
        </div>

        <?php include "./../include/side_nav.php"; ?>

    </div>

    <?php include "./../include/scripts.php"; ?>
</body>

</html>