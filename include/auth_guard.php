<?php

if (!isset($_SESSION["email"])) {
    header("Location: /ondra-wa/login.php");
    die();
}
