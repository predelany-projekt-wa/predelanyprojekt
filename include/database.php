<?php

$host = "localhost";
$port = 3306;
$user = "root";
$password = null;
$database = "wa_2023";

$conn = new mysqli($host, $user, $password, $database, $port);
mysqli_set_charset($conn, "utf8mb4");

if (mysqli_connect_error()) {
    die("Chyba s databází!");
}

return $conn;
