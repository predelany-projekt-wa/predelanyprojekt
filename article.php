<?php
require "./include/general.php";
require "./include/database.php";

$id = mysqli_real_escape_string($conn, $_GET['id']);
$article = mysqli_query($conn, "SELECT * FROM article WHERE id='$id'");
$article = mysqli_fetch_assoc($article);

?>





<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<?php include "./include/head.php"; ?>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <a href="index.php" class="logo"><strong>Vše o PC</strong> - Ondřej Voves</a>
                </header>

                <section>
                    <header class="main">
                        <h1>Seznam článků</h1>
                        <p>Zde naleznete veškteré články</p>
                    </header>
                    <hr class="major" />

                    <article>
                        <h2><?php echo $article["title"] ?></h2>
                        <p><?php echo $article["content"] ?></p>

                    </article>

                </section>

            </div>
        </div>

        <?php include "./include/sidenav.php"; ?>

    </div>

    <?php include "./include/scripts.php"; ?>
</body>

</html>